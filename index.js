const express = require('express')
const app = express()
const http = require('http')
const server = http.createServer(app)
const { Server } = require("socket.io")
const path = require('path')
const io = new Server(server)
const port = 3000
app.use(express.json())

//Store stock prices, with init value

const stockMap = new Map([
    ["ABC Traders",  44.51],
    ["Trendy Fin Tech", 23.99],
    ["SuperCorp", 126.91]
])

// when patch request to /update,
//emit timestamp and stock data to main page 
app.patch('/update', (req, res) => {
    const { data } = req.body
    if (stockMap.has(data.stockName)){                                       // check if stack name exist in stockMap
        stockMap.set(data.stockName, data.price)                             //if exist, set new value
        const timestamp = Date()                                
        io.emit('timestamp', timestamp)
       io.emit('stock message', data)   
    } else {
        console.log("Error: Input Stockname is not found in list")          // error message is stockname is not found
    }
    res.json(data)

})


// get '/' shall be served with index.html
app.get('/', (req,res) => {
    res.sendFile(path.join(__dirname, 'public','index.html'));
})

// get /current shall be served with stockMap in JSON file.
app.get('/current', (req,res) => {
    const stockObject = Object.fromEntries(stockMap)
    res.status(200).json(stockObject)
})


server.listen(port, () => console.log('Server is runnning on port: ', port))
